# adding chart
import matplotlib.pyplot as plt

def add_chart(data, title, x_label, y_label):
    plt.figure(figsize=(10, 6))
    plt.plot(data)
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.grid(True)
    plt.show()

def main():
    data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    add_chart(data, "My Chart", "X Axis", "Y Axis") 

if __name__ == "__main__":
    main()
                 