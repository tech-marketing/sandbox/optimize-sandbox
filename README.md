The [Optimize group](https://about.gitlab.com/handbook/product/categories/#optimize-group) is using this project as a sandbox for demos and solution validation. 

## Optional projects for sandboxing upcoming features:

1. https://gitlab.com/sam-s-test-group/analytics-demos/hosted_html
1. https://gitlab.com/groups/gl-demo-ultimate-paporte
1. https://gitlab.com/gl-demo-ultimate-lsoyris